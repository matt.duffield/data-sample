import 'jQuery';
import 'bootstrap';
import {DataService} from './services/data-service';

/**
 * Version 3! This is pretty cool!
 * Yet another change in my code....
 */
export class App {
  static inject = [DataService];

  message = 'Query By Example';
  subMessage = 'Similar to GraphQL without all the ceremony...';
  /*
    Collections - can be paged
    Fields - can be sorted and filtered
  */
  query = `
customers(limit:5,skip:0)
  firstName(sort:1)
  lastName(sort:1)
  orders
    product(filter:'minecraft',filterOptions:'i',sort:1)
    datePurchased
    quantity
    unitPrice
  `.trim();
  query2 = `
customers(limit:5,skip:0)
  firstName(filter:'patrick',filterOptions:'i',sort:1)
  lastName(sort:1)
  orders
    product(filter:'minecraft',filterOptions:'i',sort:1)
    datePurchased
    quantity
    unitPrice  
  `;
  query3 = `
customers(limit:2,skip:2)
  firstName(sort:1)
  lastName(sort:1)
  orders
    product(filter:'minecraft',filterOptions:'i',sort:1)
    datePurchased
    quantity
    unitPrice  
  `;
  // This performs a multiple match filter on firstName
  query4 = `
customers
  firstName(filter:'rob|scott',filterOptions:'i',sort:1)
  lastName(sort:1)
  orders
    product(sort:1)
    datePurchased
    quantity
    unitPrice  
  `;
  constructor(dataService) {
    this.dataService = dataService;
  }
  
  parse() {
    console.log(this.query);
    let lines = this.query
      .trim()
      .split('\n');
    // console.log('lines:', lines.length);
    let qry = [];
    let len = lines.length;
    let parent = '';
    lines.forEach((l, i) => {
      let propType = "field";
      let indent = l.search(/\S|$/);
      let nextLineNum = i + 1;
      if (nextLineNum < len) {
        let nextLine = lines[nextLineNum];
        let nextIndent = nextLine.search(/\S|$/);
        if (nextIndent > indent) {
          parent = l.trim();
          propType = "collection";
        }
      }
      let param = '';
      let limit = '';
      let skip = '';
      let sort = '';
      let filter = '';
      let filterOptions = '';
      let regex = /(\(.*\))/;
      let parentParam = '';
      let parentParams = parent.match(regex);
      let params = l.trim()
        .match(regex);
      if (parentParams) {
        parentParam = parentParams[1];
      }
      if (params) {
        param = params[1];
        let p = param
          .replace('(', '')
          .replace(')', '')
          .split(',');
        p.forEach(x => {
          let sub = x.split(':');
          if (sub[0] === 'limit') {
            limit = sub[1];
          } else if (sub[0] === 'skip') {
            skip = sub[1];
          } else if (sub[0] === 'sort') {
            sort = sub[1];
          } else if (sub[0] === 'filter') {
            filter = sub[1];
          } else if (sub[0] === 'filterOptions') {
            filterOptions = sub[1];
          }
        });
      }
      let f = {
        name: l,
        field: l.trim().replace(param, ''),
        parent: parent.replace(parentParam, ''),
        params: param,
        limit: limit,
        skip: skip,
        sort: sort,
        filter: filter
          .replace(/'/g, '')
          .replace(/"/g, ''),
        filterOptions: filterOptions
          .replace(/'/g, '')
          .replace(/"/g, ''),
        indent: indent, 
        propType: propType
      };
      qry.push(f);
    });
    this.process(qry);
  }
  /**
   *  This function limits in a query object and produces a valid
   *  MongoDB REST API query.
   *  It supports:
   *  Projection - (1 - include; 0 - exclude)
   *  Sorting - (1 - ascending; -1 - descending)
   *  Filtering - 
   *  Paging - (skip;limit) 
   */
  process(qry) {
    // this.output = JSON.stringify(qry, null, 2);
    console.log([...qry]);
    let coll = qry.shift();
    let collection = coll.field;
    let filter = {};
    let orderBy = {};
    let fields = {};
    let limit = coll.limit;
    let skip = coll.skip;
    qry.forEach(q => {
      if (q.indent === 2) {
        fields[q.field] = 1;
        if (q.sort) {
          orderBy[q.field] = parseInt(q.sort);
        }
        if (q.filter) {
          filter[q.field] = {
            "$regex": q.filter,
            "$options": q.filterOptions
          };
          if (!q.filterOptions) {
            delete filter[q.field].$options;
          }
        }
      } else if (q.indent > 2) {
        let fn = `${q.parent}.${q.field}`;
        fields[fn] = 1;
        if (q.sort) {
          orderBy[fn] = parseInt(q.sort);
        }
        if (q.filter) {
          filter[fn] = {
            "$regex": q.filter,
            "$options": q.filterOptions
          };
          if (!q.filterOptions) {
            delete filter[fn].$options;
          }
        }
      }
    });
    this.run({
      database: 'fec-playground',
      collection,
      filter,
      // filter: {"firstName":{"$regex":"rob|scott","$options":"i"}},
      orderBy,
      fields,
      limit,
      skip
    });
  }
  run(options) {
    this.dataService.findAll(
      options.database, 
      options.collection, 
      options.filter, 
      options.orderBy, 
      options.fields, 
      options.limit, 
      options.skip
    ).then(result => {
      console.log('count: ', result.length);
      // We sort all sub-collections once they have returned.
      let os = Object.keys(options.orderBy).filter(k => k.indexOf('.') > 0);
      os.forEach(o => {
        result.forEach(r => {
          let parent = o.split('.').shift();
          let cmp = (a,b) => {
            if (isNaN(a) || isNaN(b)) {
              if (a > b) return 1;
              if (a < b) return -1;
              return 0;
            } else {
              return a - b;
            }
          };
          let direction = options.orderBy[o];
          let prop = o.split('.').pop();
          if (direction === 1) {
            r[parent] = r[parent].sort((a, b) => cmp(a[prop], b[prop]));
          } else {
            r[parent] = r[parent].sort((a, b) => cmp(a[prop], b[prop])).reverse();            
          }
        });
      });
      this.output = JSON.stringify(result, null, 2);
    });
  }
  
}