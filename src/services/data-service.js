import {HttpClient, json} from 'aurelia-fetch-client';

export class DataService {

  url = 'databases/';
  baseUrl = `https://api.mlab.com/api/1/${this.url}`;
  apiKey = 'apiKey=GDOWW9TNkcG5xZC45XcKXm6gFIjdBWcE';

  modal = {
    metaSrc: '',
    metaKey: '',
    dataSrc: '',
    dataKey: '',
    title: '',
  };
  local_cache = {};

  constructor() {
    this.http = new HttpClient();
    this.http.configure(config => {
      config
        .withBaseUrl(this.baseUrl)
        .withDefaults({
          headers: {
            'Accept': 'application/json',
            'X-Requested-With': 'Fetch'
          }
        })
        .withInterceptor({
          request(request) {
            // console.log(`Requesting ${request.method} ${request.url}`);
            return request; // you can return a modified Request, or you can short-circuit the request              
          },
          response(response) {
            // console.log(`  Received ${response.status} ${response.url}`);
            return response; // you can return a modified Response
          }
        });
    });
  }
  

  queryByExample(values) {
    return function(item) {
      let keys = Object.keys( values );
      let answer = true;

      for( var i = 0, len = keys.length; i < len; i++) {
        if( item[keys[i]] !== values[keys[i]] ) {
          answer = false;
          break;
        }
      }
      return answer;
    }
  }
  /**
   *  This method takes in a database, collection, and id
   *  and tries to see if a cache exists. Otherwise, it 
   *  performs a fetch from the database to see if an object
   *  exists. Finally, it adds the result to the cache for
   *  any subsequent calls.
   */
  findById(database, collection, id) {
    // console.log('findById', id);
    let cache = this.findByIdFromCache(database, collection, id);
    if (cache) return Promise.resolve(cache);
    // /databases/{database}/collections/{collection}/{_id}
    return this.http.fetch(`${database}/collections/${collection}/${id}?${this.apiKey}`)
      .then(response => response.json())
      .then(data => {
        // console.log(data);
        this.createCache(database, collection, null, null, data);
        return data;
      });
  }
  /**
   *
   */
  findOne(database, collection, filter) {
    let cache = this.getOneFromCache(database, collection, filter);
    if (cache) return Promise.resolve(cache);
    let query = '?fo=true&';
    if (filter) {
      query = `?q=${JSON.stringify(filter)}&fo=true&`;
      // console.log('filter', query);
    }
    return this.http.fetch(`${database}/collections/${collection}${query}${this.apiKey}`)
      .then(response => response.json())
      .then(data => {
        // console.log(data);
        this.createCache(database, collection, filter, null, data);
        return data;
      });
  }
  /**
   *  This method takes in a database, collection, filter,
   *  and orderBy and tries to see if a cache exists. 
   *  Otherwise, it performs a fetch from the database to 
   *  see if an object exists. Finally, it adds the result
   *  to the cache for any subsequent calls.
   */
  findAll(database, collection, filter, orderBy, fields, limit, skip) {
    // let cache = this.findAllFromCache(database, collection, filter, orderBy);
    // if (cache) return Promise.resolve(cache);
    let query = '?';
    let sort = '';
    let projection = '';
    let take = '';
    let offset = '';
    if (filter) {
      query = `?q=${JSON.stringify(filter)}&`;
      // console.log('filter', query);
    }
    if (orderBy) {
      sort = `s=${JSON.stringify(orderBy)}&`;
    }
    if (fields) {
      projection = `f=${JSON.stringify(fields)}&`;
    }
    if (limit) {
      take = `l=${limit}&`;
    }
    if (skip) {
      offset = `sk=${skip}&`;
    }
    let uri = `${database}/collections/${collection}${query}${sort}${projection}${take}${offset}`;
    console.log(uri);
    return this.http.fetch(`${uri}${this.apiKey}`)
      .then(response => response.json())
      .then(data => {
        // console.log(collection, data);
        // this.createCache(database, collection, filter, orderBy, data);
        return data;
      });
  }
  /**
   *  This method takes in a database, collection, and data and
   *  tries to insert the data into the database. It then adds
   *  the returned object to the cache.
   */
  insert(database, collection, data) {
    return this.http.fetch(`${database}/collections/${collection}?${this.apiKey}`, {
        method: 'post',
        body: json(data)
      })
      .then(response => response.json())
      .then(data => {
        // console.log(data);
        // Insert cache
        this.insertCache(database, collection, null, null, data);
        return data;
      });    
  }
  /**
   *  This method takes in a database, collectin, filter, and
   *  data and tries to update the data in the database. It then
   *  updates the object in the cache.
   */
  update(database, collection, filter, data) {
    let query = '?';
    if (filter) {
      query = `?q=${JSON.stringify(filter)}&`;
      // console.log('filter', query);
    }
    return this.http.fetch(`${database}/collections/${collection}${query}${this.apiKey}`, {
        method: 'put',
        body: json(data)
      })
      .then(response => response.json())
      .then(upd => {
        // console.log(data);
        // Update cache
        this.updateCache(database, collection, filter, null, data);
        return upd;
      });    
  }
  /**
   *  This method takes in a database, collection, and id and tries
   *  to delete the object from the database. It then tries to 
   *  delete the same object from the cache.
   */
  deleteOne(database, collection, id) {
    return this.http.fetch(`${database}/collections/${collection}/${id}?${this.apiKey}`, {
        method: 'delete'
      })
      .then(response => response.json())
      .then(data => {
        // console.log(data);
        // Flush cache by id
        this.flushCacheById(database, collection, id);
        return data;
      });    
  }
  /**
   *  This method takes in a database, collection, and filter and
   *  tries to delete the object(s) from the database. It then tries 
   *  to delete the same object(s) from the cache.
   */
  deleteAll(database, collection, filter) {
    let query = '?';
    if (filter) {
      query = `?q=${JSON.stringify(filter)}&`;
      // console.log('filter', query);
    }    
    return this.http.fetch(`${database}/collections/${collection}${query}${this.apiKey}`, {
        method: 'put',
        body: json([])
      })
      .then(response => response.json())
      .then(data => {
        // console.log(data);
        // Flush all from cache by filter
        this.flushCache(database, collection, filter);
        return data;
      });    
  }
  /**
   *  This method takes in a database, collection, and data and 
   *  tries to either update or insert the data in the database
   *  by calling the appropriate helper method. 
   */
  save(database, collection, data) {
    if (data._id && data._id.$oid) {
      let filter = { _id: data._id };
      return this.update(database, collection, filter, data);
    } else {
      delete data._id;
      return this.insert(database, collection, data);
    }
  }

}